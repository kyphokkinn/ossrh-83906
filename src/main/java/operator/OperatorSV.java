package operator;

/*
 * author: kangto
 * createdAt: 29/08/2022
 * time: 11:00
 */
public interface OperatorSV {

    int sumInt(int a, int b);

}
