package operator;

/*
 * author: kangto
 * createdAt: 29/08/2022
 * time: 11:00
 */
public class OperatorIP implements OperatorSV {
    @Override
    public int sumInt(int a, int b) {
        return a+b;
    }
}
